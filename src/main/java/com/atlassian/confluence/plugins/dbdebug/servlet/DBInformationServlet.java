package com.atlassian.confluence.plugins.dbdebug.servlet;

import com.atlassian.confluence.upgrade.upgradetask.DataAccessUtils;
import com.google.common.base.Function;
import com.google.common.collect.Sets;
import net.sf.hibernate.SessionFactory;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.PlatformTransactionManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.collect.Lists.newArrayList;

public class DBInformationServlet extends HttpServlet {

    private static final String LASTMODIFIER = "LASTMODIFIER";
    private static final String CREATOR = "CREATOR";
    private static final String USERNAME = "USERNAME";
    private static final String OWNER = "OWNER";
    private static final String PERMUSERNAME = "PERMUSERNAME";
    private final SessionFactory sessionFactory;
    private final PlatformTransactionManager transactionManager;
    private final Map<String,Set<String>> userNameReferences;

    public DBInformationServlet(SessionFactory sessionFactory, PlatformTransactionManager transactionManager)
    {
        this.sessionFactory = sessionFactory;
        this.transactionManager = transactionManager;
        this.userNameReferences = makeUserNameReferences();
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "DB Information";
        String docType =
                "<!doctype html>\n";
        out.println(docType +
                "<html>\n" +
                "<head><title>" + title + "</title></head>\n" +
                "<body bgcolor=\"#f0f0f0\">\n" +
                "<h1 align=\"center\">" + title + "</h1>\n" +
                "<table width=\"100%\" border=\"1\" align=\"center\">\n" +
                "<tr bgcolor=\"#949494\">\n" +
                "<th>Table Name</th><th>Table Primary Keys</th>\n" +
                "</tr>\n");

        Map<String,Set<String>> existingUserNameReferences = filterToExistingTablesOnly(userNameReferences);
        for (Map.Entry<String, Set<String>> entry : existingUserNameReferences.entrySet())
        {
            final String table = entry.getKey();
            out.print("<tr>\n<td>\n" + table + "</td>\n<td>");

            String primaryKeyColumnName = DBInformationServlet.getPrimaryKeyColumnName(transactionManager, sessionFactory, table);
            out.print(primaryKeyColumnName);
            out.print("</td>\n</tr>\n");
        }

        out.println("</tr>\n</table>\n</body></html>");
    }

    private Map<String,Set<String>>  makeUserNameReferences()
    {
        Map<String,Set<String>> result = new HashMap<String, Set<String>>();
        result.put("ATTACHMENTS", Sets.newHashSet(CREATOR, LASTMODIFIER));
        result.put("CONTENT", Sets.newHashSet(CREATOR, LASTMODIFIER, USERNAME));
        result.put("CONTENT_LABEL", Sets.newHashSet(OWNER));
        result.put("CONTENT_PERM", Sets.newHashSet(USERNAME, CREATOR, LASTMODIFIER));
        result.put("EXTRNLNKS", Sets.newHashSet(CREATOR, LASTMODIFIER));
        result.put("FOLLOW_CONNECTIONS", Sets.newHashSet("FOLLOWER", "FOLLOWEE"));
        result.put("LABEL", Sets.newHashSet(OWNER));
        result.put("logininfo", Sets.newHashSet(USERNAME));
        result.put("LIKES", Sets.newHashSet(USERNAME));
        result.put("LINKS", Sets.newHashSet(CREATOR, LASTMODIFIER));
        result.put("NOTIFICATIONS", Sets.newHashSet(USERNAME, CREATOR, LASTMODIFIER));
        result.put("PAGETEMPLATES", Sets.newHashSet(CREATOR, LASTMODIFIER));
        result.put("SPACEGROUPPERMISSIONS", Sets.newHashSet(PERMUSERNAME));
        result.put("SPACEGROUPS", Sets.newHashSet(CREATOR, LASTMODIFIER));
        result.put("SPACEPERMISSIONS", Sets.newHashSet(PERMUSERNAME, CREATOR, LASTMODIFIER));
        result.put("SPACES", Sets.newHashSet(CREATOR, LASTMODIFIER));
        result.put("TRACKBACKLINKS", Sets.newHashSet(CREATOR, LASTMODIFIER));
        return result;
    }

    private Map<String,Set<String>> filterToExistingTablesOnly(final Map<String,Set<String>> tableKeys)
    {
        Set<String> tableNames = new HashSet<String>(tableKeys.keySet());
        tableNames = DataAccessUtils.filterToExistingTables(tableNames, transactionManager, sessionFactory);

        Map<String,Set<String>> filteredTablesAndKeys = new HashMap<String,Set<String>>(tableNames.size());

        for (String tableName : tableNames)
        {
            filteredTablesAndKeys.put(tableName, tableKeys.get(tableName));
        }

        return filteredTablesAndKeys;
    }

    public static String getPrimaryKeyColumnName(final PlatformTransactionManager transactionManager, final SessionFactory sessionFactory, final String tableName)
    {
        return DataAccessUtils.withNewConnection(transactionManager, sessionFactory, new Function<Connection, String>()
        {
            @Override
            public String apply(Connection connection)
            {
                try
                {
                    DatabaseMetaData metaData = connection.getMetaData();
                    String tableNameIdentifier = tableName;
                    if (metaData.storesLowerCaseIdentifiers())
                        tableNameIdentifier = tableName.toLowerCase();
                    if (metaData.storesUpperCaseIdentifiers())
                        tableNameIdentifier = tableName.toUpperCase();

                    ResultSet rs = metaData.getPrimaryKeys(null, null, tableNameIdentifier);
                    try
                    {
                        final List<String> primaryKeyColumnNames = newArrayList();
                        while (rs.next())
                        {
                            primaryKeyColumnNames.add(rs.getString("COLUMN_NAME"));
                        }

                        return primaryKeyColumnNames.toString();
                    }
                    finally
                    {
                        rs.close();
                    }
                }
                catch (SQLException e)
                {
                    throw new UncategorizedSQLException("Failed to retrieve primary column name for table " + tableName, "", e);
                }
            }
        });
    }
}
